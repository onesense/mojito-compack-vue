import Vue from 'vue'
import Map from './index.vue';

export default {
  title: 'Test/Map',
  component: Map,
  argTypes: {
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { Map },
  template: '<Map v-bind="$props" />',
});

export const Primary = Template.bind({});
Primary.args = {
  styles: {
    width: 300,
    height: 300
  },
  center:"杭州"
};
